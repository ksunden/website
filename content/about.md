---
date: 2022-11-07
---

Kyle is a Research Software Engineer (RSE) working for Matplotlib.
Kyle is a PhD graduate from the Wright Group at the University of Wisconsin—Madison.

[GPG public key](https://ksunden.space/publickey.gpg)
