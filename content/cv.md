---
date: 2022-03-04
---

# Kyle Sunden

Madison, WI

(269) 615-0174

contact@ksunden.space

## Education

### University of Wisconsin–Madison
*2016-present*

In pursuit of a **PhD in Analytical Chemistry**

GPA: 3.83 [transcript](/docs/wisc_transcript.pdf)

### Kalamazoo College
*2010-2016*

BA in Chemistry (ACS Approved Curriculum)

BA in Computer Science

Minor in Mathematics

F.W. Heyl Scholar *Full-ride academic scholarship awarded for excellence in mathematics and sciences*

Enrolled concurrent with High School as a Dual Enrollment student from 2010-2012.

*Magna Cum Laude*

Honors in Chemistry

Honors in Computer Science

GPA: 3.90 [transcript](/docs/kalamazoo_transcript.pdf)


### Gull Lake High School
*2008-2012*

### Kalamazoo Area Mathematics and Science Center
*2008-2012*

## Experience

### Graduate Research Assistant: Spectroscopy
*2016-Present* **Madison, WI**

[John C. Wright Group](https://wright.chem.wisc.edu)

- Maintained software infrastructure for data acquisition, representation, manipulation, and simulation
- All software work is open source and available on [GitHub](https://github.com/wright-group) or [GitLab](https://gitlab.com/yaq)

### Undergraduate Researcher: Computational Chemistry
*2014-2016*  **Kalamazoo, MI**

[Laura Lowe Furge Group](https://reason.kzoo.edu/chem/faculty/laura/)

- Performed computational analysis of human Cytochrome P450 2D6, a drug metabolizing enzyme
- Used the AMBER 14 molecular dynamics suite with Pytraj and CAVER 3.0 to analyze results
- Completed a Senior Individualized Project, investigating polymorphic variants and their interactions with bufuralol, a substrate of CYP2D6
- Published analysis of mechanism based inactivation in PLoS ONE (DOI: [10.1371/journal.pone.0108607](https://dx.doi.org/10.1371/journal.pone.0108607))
- Presented findings to the Great Lakes Drug Metabolism & Disposition Group, May 2015

### Teaching Assistant: Computer Science
*2013-2016*  **Kalamazoo, MI**

[Kalamazoo College Computer Science Department](http://reason.kzoo.edu/cs/)

- Departmental Student Advisor, chosen by the department to serve as peer resource to underclassmen as they explore academic options
- Aid in-class activities for Introduction to Computer Science, Pictures and Sounds, Introduction to Programming, Data Structures, Mobile Computing, and Building the Internet
- Hold regular office hours to assist and teach students in all classes in the department

### Laboratory Teaching Assistant: Chemistry
*2014-2015*  **Kalamazoo, MI**

[Kalamazoo College Chemistry Department](http://reason.kzoo.edu/chem/)

- Selected by Dr. Stevens-Truss to share knowledge of computational methods for medicinal chemistry laboratory, including visualizing protein structure and docking substrates with Autodock Vina
- Oversaw laboratory experiments for introductory chemistry

## Open Source Contributions

### Wright Group Projects

- **[WrightTools](https://github.com/wright-group/WrightTools)**
    - Primary maintainer
    - HDF5 based library for representing and manipulating multidimensional datasets
    - Read data from commercial manufacturers and custom instruments into a standardized format
    - Tools for combining, splitting, and adjusting multidimensional datasets
    - Provide easy plotting for rapid data quality assessment and for publication quality figures

- **[attune](https://github.com/wright-group/attune)**
    - Primary maintainer, original author
    - Represent tuning curves for Optical Parametric Amplifiers
    - Record a history of changes to the tuning curves

- **[yaqc-cmds](https://github.com/wright-group/attune)**
    - Primary maintainer
    - Custom GUI program to perform multidimensional spectroscopy acquisitions
    - Provides a queue of acquisitions
    - Allows for acquisition queue manipulation including adding new acquisitions and reordering
    - Provides live plots for rapid feedback as data is collected
    - Records data into the WrightTools "wt5" format

- **[bluesky-cmds](https://github.com/wright-group/attune)**
    - Primary maintainer, original author
    - forked from yaqc-cmds, modified to use bluesky-queueserver
    - Allows for acquisition queue manipulation including adding new acquisitions and reordering
    - Provides live plots for rapid feedback as data is collected
    - Is decoupled from the acquisition/recording process, and thus can be closed without consequence

- **[WrightSim](https://github.com/wright-group/attune)**
    - Original author
    - Simulate experiments performed by the Wright Group
    - Used algorithmic improvements to gain an order of magnitude improvement over previous implementation
    - (Optional) CUDA implementation to provide an additional order of magnitude performance
    - Recognized as one of the top three projects in the Fall 2017 University of Wisconsin High Performance Computing class

### [yaq](https://yaq.fyi)

### Bluesky Collaboration Projects

- **[yaqc-bluesky](https://github.com/bluesky/yaqc-bluesky)**
    - Maintainer
    - Implement a Bluesky device wrapping a yaq daemon
    - Map yaq concepts into the Bluesky abstraction

- **[wright-plans](https://github.com/wright-group/wright-plans)**
    - Maintainer and Original Author
    - Bluesky plans to perform tasks required by the Wright Group
    - Enhanced Unit support for many default Bluesky plans
    - Added capability to set additional hardware to track linear combinations of other hardware
    - Implement specialized plans for tuning Optical Parametric Amplifiers

- **[bluesky-hwproxy](https://github.com/ksunden/bluesky-hwproxy)**
    - Maintainer and Original Author
    - Provide TCP based access to bluesky device information

- **[bluesky](https://github.com/bluesky/bluesky)**
    - Contributor
    - Wrote the PEP 544 compliant Protocol classes to allow duck typing with static type checks for common bluesky device behavior

- **[bluesky-queueserver](https://github.com/bluesky/bluesky-queueserver)**
    - Early adopter/tester

- **[bluesky-in-a-box](https://github.com/wright-group/bluesky-in-a-box)**
    - Collection of docker containers to run the services required for bluesky-queueserver
    - Uses Docker Compose to coordinate dependencies
    - Includes running Mongo and Redis servers
    - Includes custom data writer to the WrightTools "wt5" data format

### Scientific Python Ecosystem

- **[pyqtgraph](https://pyqtgraph.org)**
    - Maintainer, Release Manager
    - Focus on easing maintenance burden of the code base
        - Use static analysis tools to find unused or broken code
	- Use automation to ease release procedure
	- Rework CI to use Github Actions
    - Relieve PR backlog through code review and resolving merge conflicts

- **[matplotlib](https://github.com/matplotlib/matplotlib/pulls?q=is%3Apr+author%3Aksunden)**
    - Contributor
    - Test pre-release code against my libraries to catch bugs/deprecations early
    - Track and fix bugs when found

- **[numpy](https://github.com/numpy/numpy/pulls?q=is%3Apr+author%3Aksunden)**
    - Contributor
    - Fixed 128-bit float printing on ARM architecture

- **[JOSS Reviewer](https://github.com/openjournals/joss-reviews/)**
    - Reviewer for [spectrapepper](https://github.com/openjournals/joss-reviews/issues/3781)

- **[Conda Forge Packaging](https://github.com/wright-group/attune)**
    - Update Conda Forge packages for most of the projects I maintain

- **Many smaller bug fixes/documentation fixes**
    - [h5py](https://github.com/h5py/h5py/pulls?q=is%3Apr+author%3Aksunden)
    - [flit](https://github.com/pypa/flit/pulls?q=is%3Apr+author%3Aksunden)
    - [fastavro](https://github.com/fastavro/fastavro/pulls?q=is%3Apr+author%3Aksunden)
    - [sphinx-gallery](https://github.com/sphinx-gallery/sphinx-gallery/pulls?q=is%3Apr+author%3Aksunden)
    - [unyt](https://github.com/yt-project/unyt/pulls?q=is%3Apr+author%3Aksunden)




## Publications

1. **[WrightTools: a Python package for multidimensional spectroscopy](https://doi.org/10.21105/joss.01141)**

    _Thompson, B. J.; **Sunden, K. F.**; Morrow, D. J.; Kohler, D. D.; and Wright, J. C._

    The Journal of Open Source Software (2019)

    DOI: 10.21105/joss.01141

    Github: [WrightTools](https://github.com/wright-group/WrightTools)

    <details><summary>BibTeX Citation</summary>

    ```
    @article{Thompson_2019,
     author = {Thompson, Blaise and Sunden, Kyle and Morrow, Darien and Kohler, Daniel and Wright, John},
     title = {WrightTools: a Python package for multidimensional spectroscopy},
     journal = {Journal of Open Source Software},
     publisher = {The Open Journal},
     year = {2019},
     month = {jan},
     number = {33},
     volume = {4},
     pages = {1141},
     url = {https://doi.org/10.21105%2Fjoss.01141},
     doi = {10.21105/joss.01141}
    }
    ```
    </details>

1. **[Three Dimensional Triply Resonant Sum Frequency Spectroscopy Revealing Vibronic Coupling in Cobalamins: Toward a Probe of Reaction Coordinates](https://doi.org/10.1021/acs.jpca.8b07678)**

    _Handali, J. D.; **Sunden, K. F.**; Thompson, B. J.; Neff-Mallon, N. A.; Kaufman, E. M.; Brunold, T. C.; and Wright, J. C._

    The Journal of Physical Chemistry A (2018)

    DOI: 10.1021/acs.jpca.8b07678

    OSF: https://osf.io/x4w2p
    
    <details><summary>BibTeX Citation</summary>
    ```
    @article{Handali_2018c,
     author = {Handali, Jonathan D. and Sunden, Kyle F. and Thompson, Blaise J. and Neff-Mallon, Nathan A. and Kaufman, Emily M. and Brunold, Thomas C. and Wright, John C.},
     title = {Three Dimensional Triply Resonant Sum Frequency Spectroscopy Revealing Vibronic Coupling in Cobalamins: Toward a Probe of Reaction Coordinates},
     journal = {The Journal of Physical Chemistry A},
     publisher = {American Chemical Society (ACS)},
     year = {2018},
     month = {oct},
     number = {46},
     volume = {122},
     pages = {9031--9042},
     url = {https://doi.org/10.1021%2Facs.jpca.8b07678},
     doi = {10.1021/acs.jpca.8b07678}
    }
    ```
    </details>

1. **[Interference and phase mismatch effects in coherent triple sum frequency spectroscopy](https://doi.org/10.1016/j.chemphys.2018.05.023)**

    _Handali, J. D.; **Sunden, K. F.**; Kaufman, E. M.; and Wright, J. C._

    Chemical Physics (2018)

    DOI: 10.1016/j.chemphys.2018.05.023

    OSF: https://osf.io/m9ycr

    <details><summary>BibTeX Citation</summary>
    ```
    @article{Handali_2018,
     author = {Handali, Jonathan D. and Sunden, Kyle F. and Kaufman, Emily M. and Wright, John C.},
     title = {Interference and phase mismatch effects in coherent triple sum frequency spectroscopy},
     journal = {Chemical Physics},
     publisher = {Elsevier BV},
     year = {2018},
     month = {aug},
     volume = {512},
     pages = {13--19},
     url = {https://doi.org/10.1016%2Fj.chemphys.2018.05.023},
     doi = {10.1016/j.chemphys.2018.05.023}
    }
    ```
    </details>

1. **[WrightSim: Using PyCUDA to Simulate Multidimensional Spectra](http://doi.org/10.25080/Majora-4af1f417-00c)**

    _**Sunden, K. F.**; Thompson B. J.; and Wright, J. C._

    Proceedings of the 17th Python in Science Conference (2018)

    DOI: 10.25080/Majora-4af1f417-00c

    Github: [WrightSim](https://github.com/wright-group/WrightSim)
    
    <details><summary>BibTeX Citation</summary>
    ```
    @inproceedings{Sunden_2018,
     author = {Sunden, Kyle and Thompson, Blaise and Wright, John},
     title = {WrightSim: Using PyCUDA to Simulate Multidimensional Spectra},
     publisher = {SciPy},
     year = {2018},
     url = {https://doi.org/10.25080%2Fmajora-4af1f417-00c},
     doi = {10.25080/majora-4af1f417-00c},
     booktitle = {Proceedings of the 17th Python in Science Conference}
    }
    ```
    </details>

1. **[Molecular Dynamics of Human Cytochrome P450 2D6 to Investigate Effects of Polymorphic Variation on Metabolism of Bufuralol](http://hdl.handle.net/10920/30372)**

    _**Sunden, K. F.**; and Furge, L. L._

    Senior Individualized Project at Kalamazoo College (2015) 
    
    <details><summary>BibTeX Citation</summary>
    ```
    @misc{sunden_2015,
     author = {Sunden, Kyle F. and Furge, Laura Lowe},
     title = {Molecular Dynamics of Human Cytochrome P450 2D6 to Investigate Effects of Polymorphic Variation on Metabolism of Bufuralol},
     institution = {Kalamazoo College},
     year = {2015},
     url = {http://hdl.handle.net/10920/30372}
    }
    ```
    </details>

1. **[Molecular Dynamics of CYP2D6 Polymorphisms in the Absence and Presence of a Mechanism-Based Inactivator Reveals Changes in Local Flexibility and Dominant Substrate Access Channels](https://doi.org/10.1371/journal.pone.0108607)**

    _de Waal, P. W.; **Sunden, K. F.**; and Furge, L. L._

    PLoS One (2014)

    DOI: 10.1371/journal.pone.0108607
    
    <details><summary>BibTeX Citation</summary>
    ```
    @article{de_Waal_2014,
     author = {de Waal, Parker W. and Sunden, Kyle F. and Furge, Laura Lowe},
     title = {Molecular Dynamics of CYP2D6 Polymorphisms in the Absence and Presence of a Mechanism-Based Inactivator Reveals Changes in Local Flexibility and Dominant Substrate Access Channels},
     journal = {PLoS ONE},
     publisher = {Public Library of Science (PLoS)},
     year = {2014},
     month = {oct},
     number = {10},
     volume = {9},
     pages = {e108607},
     url = {http://dx.doi.org/10.1371/journal.pone.0108607},
     doi = {10.1371/journal.pone.0108607},
     editor = {Papaleo, Elena}
    }
    ```
    </details>

## Presentations


1. **[Yet Another Acquisition: The Yaq Daemon Interface for Heterogeneous Instrumentation](https://www.scipy2020.scipy.org/virtual-poster-session)**
   
    _**Sunden, K. F.**; Thompson, B. J.; Wright, J. C._

    _['Poster' Presentation](https://python.yaq.fyi/scipy-2020/)_

    19th Science in Python Conference (2020)

    Held Virtually


1. **Birds of a Feather Driving Hardware for Instrumentation**
   
    _**Sunden, K. F.**; Thompson, B. J._

    _[Panel Discussion Video](https://www.youtube.com/watch?v=6HxVbK14EDI)_

    19th Science in Python Conference (2020)

    Held Virtually


1. **[pyqtgraph project update](https://docs.google.com/presentation/d/1VJS8mMrcqM7wiMmvzQ3gEZNXVrQBUzAo9FD7uWA9o0E/edit?usp=sharing)**
   
    _**Sunden, K. F.**_

    _[Presentation Video](https://youtu.be/THloLnzIn5s?t=264)_

    19th Science in Python Conference (2020)

    Held Virtually

1. **[WrightSim: Using PyCUDA to Simulate Multidimensional Spectra](https://scipy2018.scipy.org/ehome/indexb85f.html?eventid=299527&tabid=712461&cid=2233543&sessionid=21618891&sessionchoice=1&%26)**
    
    _**Sunden, K. F.**; Thompson, B. J.; and Wright, J. C._

    _[Poster](/docs/scipy2018_poster.pdf)_

    17th Science in Python Conference (2018)

    Austin, TX, USA

1. **[Molecular Dynamics of Human CYP2D6 Provides Insights Into Structure, Function, and Susceptibility to Inactivation of Polymorphic Variants of CYP2D6](https://www.greatlakesdmdg.org/2015-meeting)**

    _**Sunden, K. F.**_

    _Oral Presentation_

    Great Lakes Drug Metabolism and Disposition Group (2015)

    Ann Arbor, MI, USA
